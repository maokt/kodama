-module(kodama_scan_debug).
-export([init/0, complete/4]).

init() -> nothing.

complete(State, Dir, Hash, _Ki) ->
    io:format("completed ~p [~p]~n", [Dir, Hash]),
    State.

