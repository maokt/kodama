-module(kodama_scan_tree).
-export([init/0, complete/4]).

init() -> gb_trees:empty().

complete(State, Dir, Hash, _Ki) -> gb_trees:insert(Dir, Hash, State).

