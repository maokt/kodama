-module(kodama_ki).
-export([make/2, diff/2]).
-include_lib("kernel/include/file.hrl").

-record(ki, {eda, ha}).
% need to convert file_info to something sensible
make(Eda, Ha) -> term_to_binary(#ki{eda=lists:keysort(1,Eda), ha=lists:keysort(1,Ha)}).

diff(Source, Target) when is_binary(Source) -> diff(binary_to_term(Source), Target);
diff(Source, Target) when is_binary(Target) -> diff(Source, binary_to_term(Target));
diff(#ki{ha=HaSrc}, #ki{ha=HaTgt}) -> {[], diff_ha(HaSrc, HaTgt)}.

diff_ha(S,T) -> diff_ha(S,T,[]).

diff_ha([], [], Acc) -> Acc;
diff_ha(Src=[], [ {Name, Fi} |Tail ], Acc) -> diff_ha(Src, Tail, [ {Name, fetch, noHash} | Acc ]);
diff_ha([ {Name, Fi} |Tail ], Tgt=[], Acc) -> diff_ha(Tail, Tgt,  [ {Name, rm, noHash} | Acc ]);
diff_ha([X|SrcTail], [X|TgtTail], Acc) -> diff_ha(SrcTail, TgtTail, Acc);
diff_ha(Src=[ {SrcName, SrcFi} |SrcTail], Tgt=[ {TgtName, TgtFi} |TgtTail], Acc) ->
    if
        SrcName < TgtName -> diff_ha(SrcTail, Tgt, [ {SrcName, rm, noHash} | Acc]);
        SrcName > TgtName -> diff_ha(Src, TgtTail, [ {TgtName, fetch, noHash} | Acc]);
        % names must match, so either chmod or hash diff; for now, return "change"
        true -> diff_ha(SrcTail, TgtTail, [ {TgtName, change, noHash} | Acc])
    end.


%munge_info(Name, #file_info{type=regular, mode=Mode, size=Size})
%  when Mode band 8#00111 =/= 0 -> {Name, exec, Size};
%munge_info(Name, #file_info{type=regular, size=Size}) -> {Name, data, Size};
%munge_info(Name, #file_info{type=symlink, size=Size}) -> {Name, symlink, Size};
%munge_info(Name, #file_info{type=directory}) -> {Name, dir, 0}.
%is_dir({_Name, #file_info{type=directory}}) -> true;
%is_dir(_) -> false.
