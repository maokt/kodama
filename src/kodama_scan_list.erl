-module(kodama_scan_list).
-export([init/0, complete/4]).

init() -> [].

complete(State, Dir, _Hash, _Ki) -> [Dir | State].

