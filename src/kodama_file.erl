-module(kodama_file).
-export([ls/1, hash_file/1, hash_link/1]).
-export([scan_dir/3]).

-include_lib("kernel/include/file.hrl").
-define(CHUNKSIZE, 262144).

ls(Dir) ->
    {ok, Entries} = file:list_dir(Dir),
    lists:map(fun(E) ->
                      {ok, FI } = file:read_link_info([Dir,$/,E], [{time,posix}]),
                      {E, FI}
              end, Entries).

is_dir({_Name, #file_info{type=directory}}) -> true;
is_dir(_) -> false.

hash_file(Path) ->
    {ok, Fh} = file:open(Path, [read, raw, binary]),
    Chunks = hash_chunks(Fh, []),
    ok = file:close(Fh),
    << <<H/binary>> || H <- Chunks>>.

hash_chunks(Fh, Chunks) ->
    case file:read(Fh, ?CHUNKSIZE) of
        {ok, Data} -> hash_chunks(Fh, [crypto:hash(sha256, Data) | Chunks]);
        eof -> lists:reverse(Chunks)
    end.

hash_link(Path) ->
    {ok, Target} = file:read_link(Path),
    crypto:hash(sha256, Target).

mk_ki(Leaves, Branches) -> term_to_binary({ki, lists:keysort(1,Leaves), lists:keysort(1,Branches)}).

scan_dir(Callback, State, Dir) ->
    % TODO Callback: check if result cached
    Ls = ls(Dir),
    % TODO Callback: filter Ls
    {SubDirs, Leaves} = lists:partition(fun is_dir/1, Ls),
    % BranchHashes is [ { ShortName, Hash } ]
    {BranchHashes, InterState} = lists:mapfoldl(
                           fun({N,_}, S0) ->
                                   {Hash,S1} = scan_dir(Callback, S0, filename:join(Dir,N)),
                                   {{N,Hash}, S1}
                           end, State, SubDirs),
    % TODO: hash the files and symlinks
    Data = mk_ki(Leaves, BranchHashes),
    FinalHash = crypto:hash(sha256, Data),
    NewState = Callback:complete(InterState, Dir, FinalHash, Data),
    {FinalHash, NewState}.

