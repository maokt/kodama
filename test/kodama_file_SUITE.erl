-module(kodama_file_SUITE).
-compile(export_all).
-include_lib("common_test/include/ct.hrl").
-include_lib("kernel/include/file.hrl").

suite() -> [{timetrap, {seconds, 20}}].
all() -> [ {group, lowlevel}, list_dirs ].
groups() -> [
             { lowlevel, [parallel], [check_ls, check_symlinks, check_small_files, check_big_file] }
            ].

init_per_suite(C) ->
    [
     { link_hashes, [
                     {"san", <<139,91,157,176,193,61,178,66,86,200,41,170,54,74,169,12,109,
                               46,186,49,139,146,50,164,171,147,19,185,84,211,85,95>>},
                     {"nowhere", <<32,160,105,217,168,216,139,213,116,34,237,246,103,57,202,
                                   218,215,207,84,85,225,7,33,151,235,48,142,35,83,87,174,29>>}
                    ]},
     { file_hashes, [
                     {"one", <<126,165,191,149,106,11,50,112,206,96,215,142,125,19,38,3,82,
                               211,62,102,22,32,210,84,38,226,57,152,11,1,12,155>>},
                     {"two", <<7,20,64,181,224,175,137,0,91,2,198,201,6,93,186,178,233,223,
                               223,109,205,236,236,140,228,76,125,162,164,85,39,168>>},
                     {"three", <<45,6,209,248,203,247,108,69,164,219,121,184,162,141,117,164,
                                 61,250,252,182,147,208,255,185,66,79,200,69,19,202,197,159>>},
                     {"four", <<202,25,203,165,103,133,12,89,82,197,46,130,7,141,107,23,11,
                                215,131,149,95,59,129,101,74,129,231,203,133,207,49,229>>}
                    ]}
     | C].

dentry_type(Ls,Name) -> #file_info{type=T} = proplists:get_value(Name, Ls), T.
check_ls(C) ->
    TopDir = ?config(data_dir, C),
    Ls = kodama_file:ls(TopDir),
    11 = length(Ls),
    directory = dentry_type(Ls, "q_dir"),
    regular = dentry_type(Ls, "hello"),
    regular = dentry_type(Ls, "three"),
    symlink = dentry_type(Ls, "san"),
    symlink = dentry_type(Ls, "nowhere").

check_symlinks(C) ->
    TopDir = ?config(data_dir, C),
    Hashes = ?config(link_hashes, C),
    lists:foreach(fun({N,H}) ->
                H = kodama_file:hash_link(filename:join(TopDir, N))
        end, Hashes).

check_small_files(C) ->
    TopDir = ?config(data_dir, C),
    Hashes = ?config(file_hashes, C),
    lists:foreach(fun({N,H}) ->
                H = kodama_file:hash_file(filename:join(TopDir, N))
        end, Hashes).

% the "big" file is the concatenation of the four small files
check_big_file(C) ->
    TopDir = ?config(data_dir, C),
    Hashes = ?config(file_hashes, C),
    Joined = << << B:32/binary >> || {_,B} <- Hashes >>,
    Joined = kodama_file:hash_file(filename:join(TopDir, "joined")).

list_dirs(C) ->
    TopDir = ?config(data_dir, C),
    Acc = kodama_scan_list:init(),
    {Hash, Dirs} = kodama_file:scan_dir(kodama_scan_list, Acc, TopDir),
    4 = length(Dirs),
    true = lists:member(TopDir, Dirs),
    true = lists:member(filename:join(TopDir, "a_dir"), Dirs),
    true = lists:member(filename:join(TopDir, "q_dir"), Dirs),
    true = lists:member(filename:join(TopDir, "z_dir"), Dirs).

