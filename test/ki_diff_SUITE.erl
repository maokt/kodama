-module(ki_diff_SUITE).
-compile(export_all).
-include_lib("common_test/include/ct.hrl").
-include_lib("kernel/include/file.hrl").

suite() -> [{timetrap, {seconds, 20}}].
all() -> [ reverse_ok, add_one, rm_one ].
% not sure if we need to detect renames here
% TODO maybe add mv_one to tests

init_per_suite(C) ->
    % use a sensible file_info record as the base for others
    % (so we don't need to populate all the info)
    {ok, FI } = file:read_link_info(".", [{time,posix}]),
    [ { base_info, FI } | C].

reverse_ok(C) ->
    Bi = ?config(base_info, C),
    Leaves = lists:map(fun(N) -> { N, Bi#file_info{} } end, ["alpha", "beta", "gamma"]),
    Before = kodama_ki:make([], Leaves),
    After = kodama_ki:make([], lists:reverse(Leaves)),
    {[], []} = kodama_ki:diff(Before, After).

add_one(C) ->
    Bi = ?config(base_info, C),
    Leaves = lists:map(fun(N) -> { N, Bi#file_info{} } end, ["alpha", "beta", "gamma"]),
    Before = kodama_ki:make([], Leaves),
    After = kodama_ki:make([], [ {"delta", noHash} | Leaves]),
    {[], [{"delta", fetch, noHash}]} = kodama_ki:diff(Before, After).

rm_one(C) ->
    Bi = ?config(base_info, C),
    Leaves = lists:map(fun(N) -> { N, Bi#file_info{} } end, ["alpha", "beta", "gamma"]),
    Before = kodama_ki:make([], Leaves),
    After = kodama_ki:make([], lists:keydelete("beta", 1, Leaves)),
    {[], [{"beta", rm, noHash}]} = kodama_ki:diff(Before, After).

mv_one(C) ->
    Bi = ?config(base_info, C),
    Leaves = lists:map(fun(N) -> { N, Bi#file_info{} } end, ["alpha", "beta", "gamma"]),
    Before = kodama_ki:make([], Leaves),
    {_Name, Fi }  = lists:keyfind("beta", 1, Leaves),
    After = kodama_ki:make([], lists:keyreplace("beta", 1, Leaves, {"delta", Fi})),
    {[], [{"beta", mv, "delta"}]} = kodama_ki:diff(Before, After).

